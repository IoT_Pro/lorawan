# README

This is LoRaWAN Project.

## Concept
LoRa is Long Range, low power wireless platform Technology.
LoRaWAN is a media access control(MAC) protocol specification built on top of the LoRa technology developed by the LoRa Alliance.

## Send data from Feather M0 to LORIOT.io website

    void initLoRaWAN() {
        // LMIC init
        os_init();
        
        // Reset the MAC state. Session and pending data transfers will be discarded.
        LMIC_reset();
        
        // by joining the network, precomputed session parameters are be provided.
        LMIC_setSession(0x1, DevAddr, (uint8_t*)NwkSkey, (uint8_t*)AppSkey);
        
        // Enabled data rate adaptation
        LMIC_setAdrMode(1);
        
        // Enable link check validation
        LMIC_setLinkCheckMode(0);
        
        // Set data rate and transmit power
        LMIC_setDrTxpow(DR_SF12, 21);
    }

    void sendStartupMessage() {
        // Ensure there is not a current TX/RX job running
        if (LMIC.opmode & (1 << 7)) {
            // Something already in the queque
            return;
        }
    
        // Put together the data to send
        char packet[41] = STARTUP_MESSAGE;
    
        // Debug message
        Serial.print("  seqno ");
        Serial.print(LMIC.seqnoUp);
        Serial.print(": ");
        Serial.println(packet);
    
        // Add to the queque
        dataSent = false;
        uint8_t lmic_packet[41];
        strcpy((char *)lmic_packet, packet);
        LMIC_setTxData2(1, lmic_packet, strlen((char *)lmic_packet), 0);
    
        // Wait for the data to send or timeout after 15s
        elapsedMillis sinceSend = 0;
        while (!dataSent && sinceSend < 15000) {
            os_runloop_once();
            delay(1);
        }
        os_runloop_once();
    }
    
    void setup() {
        
        // Setup LoRaWAN state
        initLoRaWAN();
        
        // Send Startup Message
        sendStartupMessage();
        
        // Shutdown the radio
        os_radio(RADIO_RST);
    }
    
    

## Issues
### Feather M0 Board install problem
    To use Feather M0, you install Arduino SAMD Board, Adafruit SAMD Board.


### Install Adafruit SAMD board problem in Arduino IDE
To install Adafruit SAMD board in Arduino IDE, you should add following url in File => Preferences => Additional Boards Manager URLs

    https://adafruit.github.io/arduino-board-index/package_adafruit_index.json


